#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <fstream>
#include <streambuf>

#define MAX_S_STEPS 15
#define MAX_FIELD_SIZE (MAX_S_STEPS * 2) + 1

std::string trim(const std::string& str) {

	int first = str.find_first_not_of(' ');
	if (std::string::npos == first) {
		return str;
	}
	int last = str.find_last_not_of(' ');
	return str.substr(first, (last - first + 1));
}

std::string readFileToString(std::string filename) {

	std::ifstream t(filename);
	std::string str;

	t.seekg(0, std::ios::end);
	str.reserve(t.tellg());
	t.seekg(0, std::ios::beg);
	str.assign((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

	return str;
}


void printRobotField (int arr[MAX_FIELD_SIZE][MAX_FIELD_SIZE]) {

	std::cout << "Debugging robot field" << std::endl;

	for (int i = 0; i < MAX_FIELD_SIZE; i++) {


		for (int j = 0; j < MAX_FIELD_SIZE; j++) {
	
			std::cout << arr[i][j];
		}
		std::cout << std::endl;
	}
}

int checkRobotPath(std::string robotPath) {

	int field[MAX_FIELD_SIZE][MAX_FIELD_SIZE];

	int stepsCounter = 0;     // счетчик S - шагов робота 
	int robotDirection = 0;   // направление движения: 0 - верх, 1 вправо, 2 вниз, 3 влево
	int robotX = MAX_S_STEPS; // начальные координаты робота, центр поля
	int robotY = MAX_S_STEPS; 
	int pathPosition = 0;
	int movedFromStartPoint = 0;

	int symb = 0;

	for (int i = 0; i < MAX_FIELD_SIZE; i++) {

		for (int j = 0; j < MAX_FIELD_SIZE; j++) {

			field[i][j] = 0;
		}
	}

	field[robotX][robotY] = 1;
	
	while (pathPosition < robotPath.length()) {
	
		switch (robotPath[pathPosition]) {

			case 'S': 

				  stepsCounter++;

				   switch (robotDirection) {

					 case 0: robotY--;
							break;
					 case 1: robotX++;
							break;
					 case 2: robotY++;
							break;
					 case 3: robotX--;
							break;
				   }

				  if (movedFromStartPoint > 0 && field[robotX][robotY] == 1) {

					return stepsCounter;
				  }

			  	  field[robotX][robotY] = 1;

				  movedFromStartPoint = 1;

				  break;
			case 'R': robotDirection++;
				  if (robotDirection  > 3) { robotDirection = 0; }
				  break;
			case 'L': robotDirection--;
				  if (robotDirection < 0) { robotDirection = 3; }
				  break;
		}
		pathPosition++;

		// debug ROBOT FIELD
		//printRobotField (field);
	}

	return -1;
}


int main() {

	std::cout << "K79 Robot path checker" << std::endl;
	std::string filename = "k79-input.txt";

	std::string robotPathData = readFileToString(filename);

	int robotPathResult = checkRobotPath(robotPathData); 

	std::cout << robotPathResult << std::endl;
	
	return 0;
}
