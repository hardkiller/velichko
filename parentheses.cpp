#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <fstream>
#include <streambuf>

std::string trim(const std::string& str) {

	int first = str.find_first_not_of(' ');
	if (std::string::npos == first) {
		return str;
	}
	int last = str.find_last_not_of(' ');
	return str.substr(first, (last - first + 1));
}

std::string readOperation () {

	std::string answerValue;
	std::getline(std::cin, answerValue);
	answerValue = trim(answerValue);
	std::transform(answerValue.begin(), answerValue.end(), answerValue.begin(), (int (*)(int))std::tolower);
	return answerValue;
}

bool doYouWantTryAgain(const std::string message) {

	std::string answerValue;
	std::cout << message << " (Y)es / (N)o >";
	std::getline(std::cin, answerValue);

	answerValue = trim(answerValue);

	std::transform(answerValue.begin(), answerValue.end(), answerValue.begin(), (int (*)(int))std::tolower);

	std::string y = "y";
	std::string yes = "yes";

	return (yes.compare(answerValue) == 0 || y.compare(answerValue) == 0);
}

std::string readFileToString(std::string filename) {

	std::ifstream t(filename);
	std::string str;

	t.seekg(0, std::ios::end);
	str.reserve(t.tellg());
	t.seekg(0, std::ios::beg);
	str.assign((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

	return str;
}

bool checkParenthessBalance (std::string filename) {

	std::string data = readFileToString(filename);

	int openClosuresCounter = 0;
	int closeClosuresCounter = 0;

	for (int i = 0; i < data.size(); i++) {

		if (data[i] == '(') {
			openClosuresCounter += 1;
		}

		if (data[i] == ')') {
			closeClosuresCounter += 1;
		}

		if (closeClosuresCounter > openClosuresCounter) {
			return false;
		}
	}
	return (closeClosuresCounter == openClosuresCounter);
}

int main() {

	std::cout << "Parentheses balance check" << std::endl;
	std::string filename = "";

	while(true) {

		std::cout << "Input filename >";
		filename = readOperation();

		if (checkParenthessBalance (filename) == 1) {
			std::cout << "Parentheses are balanced" << std::endl;
		} else {
			std::cout << "Parentheses are not balanced" << std::endl;
		}

		if (doYouWantTryAgain("Continue?") == 0) {
			break;
		}
	}

	return 0;
}
