#include <iostream>
#include <algorithm>
#include <string>
#include <vector>

std::string trim(const std::string& str) {

    int first = str.find_first_not_of(' ');
    if (std::string::npos == first) {
        return str;
    }
    int last = str.find_last_not_of(' ');
    return str.substr(first, (last - first + 1));
}

int readStdInInteger(int defaultValue, int minLimitValue, int maxLimitValue) {

	std::string answerValue;
	std::getline(std::cin, answerValue);
	std::string trimmedValue = trim(answerValue);

	int answer = defaultValue;

	try {
		answer = std::stoi(trimmedValue);

		if (answer < minLimitValue) {
			std::cout << "The input value must be greater than or equal " << minLimitValue << std::endl;
		}

		if (answer > maxLimitValue) {	
			std::cout << "The input value must be less or equal than " << maxLimitValue << std::endl;
		}
	} catch (...) {
		std::cout << "The entered value must be a number" << std::endl;
	}
	return answer;
}

std::string readOperation () {

	std::string answerValue;
	std::getline(std::cin, answerValue);
	answerValue = trim(answerValue);
	std::transform(answerValue.begin(), answerValue.end(), answerValue.begin(), (int (*)(int))std::tolower);
	return answerValue;
}

bool doYouWantTryAgain(const std::string message) {

	std::string answerValue;
	std::cout << message << " (Y)es / (N)o >";
	std::getline(std::cin, answerValue);

	answerValue = trim(answerValue);

	std::transform(answerValue.begin(), answerValue.end(), answerValue.begin(), (int (*)(int))std::tolower);

	std::string y = "y";
	std::string yes = "yes";

	return (yes.compare(answerValue) == 0 || y.compare(answerValue) == 0);
}

int readIntegerValueWithMinMaxLimit(int minLimitValue, int maxLimitValue,const std::string& message) {

	int defaultValue = minLimitValue - 1;
	int inputValue = defaultValue;

	while (inputValue < minLimitValue || inputValue > maxLimitValue) {

		std::cout << message << " >";

		inputValue = readStdInInteger(defaultValue, minLimitValue, maxLimitValue);

		if (inputValue < minLimitValue || inputValue > maxLimitValue) {
 
		 	if (doYouWantTryAgain("Do you want to try again?") == 0) {
				inputValue = defaultValue;
				break;
			}
		}
	}
	return inputValue;
}

std::string vectorValuesToStr (std::vector<int> elements) {

	std::string result = "";
	int lastElementNum = elements.size() - 1;
 
	for (int i = 0; i < elements.size(); i++) { 

		result += std::to_string(elements[i]);
		if (i != lastElementNum) { 
			result += ",";		
		}
	}
	return "[" + result + "]";
}

void printVectorValues (std::vector<int> elements) {

	std::string output = vectorValuesToStr (elements);
	std::cout << "Current array: " << output << std::endl;
}

int main() {

	std::cout << "Array modification" << std::endl;

	int itemsCountValue = readIntegerValueWithMinMaxLimit(1, 30, "Input the number integers");

	std::string st;
	std::vector<int> elements(itemsCountValue);

	for (int i = 0; i < itemsCountValue; i++) {
		elements[i] = readIntegerValueWithMinMaxLimit (-100, 100, "Input the integer #" + std::to_string(i + 1));
	}

	printVectorValues (elements);

	std::string operationExit = "exit";
	std::string operationInsert = "insert";
	std::string operationErase = "erase";
	std::string operation = "";

	int position = 0;
	int value = 0;

	while(true) {

		std::cout << "Input an operation (insert/erase/exit)" << std::endl;

		operation = readOperation();

		if (operationExit.compare(operation) == 0) {

			std::cout << "Exit program" << std::endl;
			break;
		}

		if (operationInsert.compare(operation) == 0) {

			position = readIntegerValueWithMinMaxLimit(1, elements.size(), "Input a position");
			value = readIntegerValueWithMinMaxLimit(-100, 100, "Input the integer #" + std::to_string(position));
			elements.insert (elements.begin() + position - 1, value);
			printVectorValues(elements);

			continue;
		}

		if (operationErase.compare(operation) == 0) {

			position = readIntegerValueWithMinMaxLimit(1, elements.size(), "Input a position");
			elements.erase (elements.begin() + position - 1); 
			printVectorValues(elements);
			continue;
		}

		std::cout << "Invalid operation" << std::endl;
	}

    return 0;
}
